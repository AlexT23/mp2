import React from 'react';
import { useState, useEffect } from 'react'
import axios from 'axios';
import { Container, Input, VStack, HStack, RadioGroup, Radio, Flex, Text, Heading } from '@chakra-ui/react';
import { useNavigate } from "react-router-dom";


export default function ListView() {    
    // Values within use state params are the initialized values
    const [order, setOrder] = useState('desc') // Ascending/Descending
    const [sort, setSort] = useState('title')   // Parameter to sort by
    const [search, setSearch] = useState('')
    const [imagesInfo, setImagesInfo] = useState('') // URLs for displayed images

    const navigate = useNavigate()
    const onImageCardClick = (imageID) => () => navigate(`/mp2/details/${imageID}`)
    
    const baseAPI = axios.create({
        baseURL: `https://api.artic.edu/api/v1/artworks/`
    })

    useEffect(() => { 
        baseAPI.get(`search?query[match][${sort}]=${search}&fields=id,title,image_id,date_start,date_end,artist_display,classification_title,is_public_domain,is_on_view&sort[_score][order]=${order}&limit=10`) 
        .then((res) => {
            if (res.data) {
                setImagesInfo(res.data.data.map(item => item))
                console.log('res data')
                console.log(res.data)
            }
        }) 
    }, [search, order, sort, baseAPI]); 

    return (
        <Flex flexFlow='column nowrap' width='55%'>
            <Heading>List View</Heading>
            <Container>
                <Input onChange= {e => (setSearch(e.target.value))}
                    placeholder='Insert Query'
                    value={search}
                />
            </Container>
            <Text>Sort by:</Text>
            <RadioGroup onChange={setSort} value={sort}>
                <HStack justifyContent='center'>
                    <Radio value='title'>Title</Radio>
                    <Radio value='artist_display'>Artist</Radio>
                </HStack>
            </RadioGroup>
            <Text>Relevance to Query:</Text>
            <RadioGroup onChange={setOrder} value={order}>
                <HStack justifyContent='center'>
                    <Radio value='desc'>Descending</Radio>
                    <Radio value='asc'>Ascending</Radio>
                </HStack>
            </RadioGroup>
            <Flex flexFlow='column nowrap' alignItems='center' id='List'>
                {
                    imagesInfo.length ? imagesInfo.map(
                        (img_info) => {
                            return (
                                <Flex flexFlow='row wrap' height='5%' width='100%' bg='yellow' border='2px' borderRadius='lg' gap={6} onClick={onImageCardClick(img_info.id)}>
                                    <VStack spacing='10px'>
                                        <Heading size='md' noOfLines={2}>{img_info.title}</Heading>
                                        <Heading size='sm' noOfLines={2}>{img_info.artist_display}</Heading>
                                        <Heading size='sm'> Relevance score: {img_info._score}</Heading>
                                        <img src={`https://www.artic.edu/iiif/2/${img_info.image_id}/full/843,/0/default.jpg`} width="80%" alt={img_info.title} />
                                        <Heading> </Heading>
                                    </VStack>
                                </Flex>
                            );
                        }
                    ) : <></>
                }
            </Flex>
        </Flex>
    );
    }
