import React from 'react';
import { useState,useEffect } from 'react'
import axios from 'axios';
import { Container, HStack, Flex, Heading, CheckboxGroup, Checkbox } from '@chakra-ui/react';
import { useNavigate } from "react-router-dom";

const FILTER_VALUES = {
    PAINTING: "painting",
    DRAWING: "drawings",         // New
    COLLAGE: "collage",             // New
    PASTEL: "pastels",       // New
    EUROPEAN: "european",   
    ASIAN: "asian",         // New
    AFRICAN: "african",     // New
    AMERICAN: "american", 
}

const BASE_API = "https://api.artic.edu/api/v1/artworks/"
export default function GalleryView() {    
    // Legacy code from list.js, potentially unnecessry
    const [imagesInfo, setImagesInfo] = useState('')    // URLs for displayed images
    const [filters, setFilters] = useState({[FILTER_VALUES.OIL]: FILTER_VALUES.OIL})
    
    const navigate = useNavigate()
    
    const handleFilterChange = (filter) => (e) => {
        setFilters({...filters, [filter]: !!e.target.checked ? e.target.checked: undefined}) // NOTE: !! double negative used for checking if undefined/null 
    }
    const baseAPI = axios.create({
        baseURL: BASE_API
    })
    const filterBuilder = () => {
        const filteredValue = Object.keys(filters).filter((filter_key) => !!filters[filter_key]).join(" ")
        return`search?query[match][term_titles][operator]=and&query[match][term_titles][query]=${filteredValue}&fields=id,title,image_id,date_start,date_end,artist_display,classification_titles&limit=20`
    }
    const getImages = async () => {
        const {data: {data} } =  await baseAPI.get(filterBuilder()) 
        // const {data: {data} } =  await baseAPI.get(`search?query[bool][filter][][terms][classification_titles.keyword]=[modern]`) 
        console.log(data)
        setImagesInfo(data.map(image => image)) 
    }

    const onImageCardClick = (imageID) => () => navigate(`/mp2/details/${imageID}`)

     
    useEffect(() => { 
        getImages()
    }, [filters]); // <--- Array argument: Only re-run effect if these arguments change. Without this, UseEffect will run on everything.

    return (
        <Flex flexFlow='column nowrap'>
            <Heading>Gallery View</Heading>
            <Container width='75%'>
            {/* <RadioGroup onChange={setSort} value={sort}>
                <HStack justifyContent='center'>
                    <Radio value='title'>Title</Radio>
                    <Radio value='artist_display'>Artist</Radio>
                </HStack>
            </RadioGroup> */}
                <CheckboxGroup defaultValue={[]} >
                    <HStack justifyContent='space-evenly'>
                        <Checkbox 
                            value={FILTER_VALUES.PAINTING} 
                            isChecked={!!filters[FILTER_VALUES.PAINTING]} 
                            onChange={(handleFilterChange(FILTER_VALUES.PAINTING))}>
                            Paintings
                        </Checkbox>
                        <Checkbox 
                            value={FILTER_VALUES.DRAWING} 
                            isChecked={!!filters[FILTER_VALUES.DRAWING]} 
                            onChange={(handleFilterChange(FILTER_VALUES.DRAWING))}>
                            Drawings
                        </Checkbox>
                        <Checkbox 
                            value={FILTER_VALUES.COLLAGE} 
                            isChecked={!!filters[FILTER_VALUES.COLLAGE]} 
                            onChange={(handleFilterChange(FILTER_VALUES.COLLAGE))}>
                            Collages
                        </Checkbox>
                        <Checkbox 
                            value={FILTER_VALUES.PASTEL} 
                            isChecked={!!filters[FILTER_VALUES.PASTEL]} 
                            onChange={(handleFilterChange(FILTER_VALUES.PASTEL))}>
                            Pastel 
                        </Checkbox>
                        <Checkbox 
                            value={FILTER_VALUES.EUROPEAN} 
                            isChecked={!!filters[FILTER_VALUES.EUROPEAN]} 
                            onChange={(handleFilterChange(FILTER_VALUES.EUROPEAN))}>
                            European
                        </Checkbox>
                        <Checkbox 
                            value={FILTER_VALUES.ASIAN} 
                            isChecked={!!filters[FILTER_VALUES.ASIAN]} 
                            onChange={(handleFilterChange(FILTER_VALUES.ASIAN))}>
                            Asian
                        </Checkbox>
                        <Checkbox 
                            value={FILTER_VALUES.AFRICAN} 
                            isChecked={!!filters[FILTER_VALUES.AFRICAN]} 
                            onChange={(handleFilterChange(FILTER_VALUES.AFRICAN))}>
                            African 
                        </Checkbox>
                        <Checkbox 
                            value={FILTER_VALUES.AMERICAN} 
                            isChecked={!!filters[FILTER_VALUES.AMERICAN]} 
                            onChange={(handleFilterChange(FILTER_VALUES.AMERICAN))}>
                            American
                        </Checkbox> 
                    </HStack>
                </CheckboxGroup>
            </Container>
            <Container justifyContent='center'>
                <Flex flexFlow='row wrap' justifyContent='center' alignItems='stretch' id='List' width='100%'>
                    {
                        imagesInfo.length ? imagesInfo.map(
                            (img_info) => { // Abstract out the card section into function w/ proptypes
                                return (
                                    <Flex flexFlow='column nowrap' alignItems='center' width='25%' bg='yellow' border='2px' borderRadius='lg' gap={6} onClick={onImageCardClick(img_info.id)}>
                                        <Heading size='md' noOfLines={2} >{img_info.title}</Heading>
                                        <Heading size='sm' noOfLines={2} >{img_info.artist_display}</Heading>
                                        <Heading size='sm' noOfLines={2} > Relevance score: {img_info._score}</Heading>
                                        <img src={`https://www.artic.edu/iiif/2/${img_info.image_id}/full/843,/0/default.jpg`} width="90%"  alt={img_info.title} />
                                        <Heading> </Heading>
                                    </Flex>
                                );
                            }
                        ) : <></>
                    }
                </Flex>
            </Container>
        </Flex>
    );
    }
