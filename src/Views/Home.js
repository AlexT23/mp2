import { Heading, VStack, Container } from "@chakra-ui/react";

export default function Home() {
    return(
        <VStack justifyContent='center'>
            <Heading> Welcome to the Virtual Gallery! </Heading>
            <Container> Click on a display option to begin! </Container>

            <Heading size='md'> Resources Used: </Heading>
            <Container> API: https://api.artic.edu/ </Container>
            <Container> https://css-tricks.com/snippets/css/a-guide-to-flexbox/ </Container>
            <Container> https://api.artic.edu/docs/ </Container>
            <Container> https://github.com/ClementCariou/virtual-art-gallery </Container>
            <Container> https://github.com/art-institute-of-chicago/data-aggregator </Container>
            <Container> https://www.elastic.co/guide/en/elasticsearch/reference/6.0/query-filter-context.html </Container>
            <Container> https://chakra-ui.com/docs/components </Container>
        </VStack>
    )
}