import axios from 'axios';
import { useParams } from 'react-router-dom';
import { useEffect, useState } from "react"
import { VStack, Heading } from '@chakra-ui/react';

export default function DetailView() {
    const [imagesInfo, setImagesInfo] = useState('')
    const {imageID} = useParams()
    
    const baseAPI = axios.create({
        baseURL: "https://api.artic.edu/api/v1/artworks/"
    })

    // IDs are not continuous - must query for next closest IDs

    // Retrieve contents of image ID
	useEffect(() => {
        // baseAPI.get(`artworks?fields=id,title,image_id,date_start,date_end,artist_display,classification_title,is_public_domain,is_on_view/${imageID}`)
        baseAPI.get(`${imageID}`)
        .then((res) => {
            console.log(imageID)
            if (res.data) {
                setImagesInfo(res.data.data)
            }
        })
	}, [baseAPI, imageID])

    return (
        <VStack>
            <Heading> Detail View</Heading>
            <VStack height='5%' width='75%' bg='yellow' border='2px' borderRadius='lg' gap={2}>
                <Heading size='lg'>{imagesInfo.title}</Heading>
                <Heading size='md'>Artist: {imagesInfo.artist_display}</Heading>
                <Heading size='md'>ID: {imagesInfo.id}</Heading>
                <img src={`https://www.artic.edu/iiif/2/${imagesInfo.image_id}/full/843,/0/default.jpg`} width="80%" alt='thumbnail' />
                <Heading size='md'> Created: {imagesInfo.date_start} - {imagesInfo.date_end}</Heading>
                <Heading size='md'> Style: {imagesInfo.style_title}</Heading>
                <Heading size='md'> Medium: {imagesInfo.medium_display} </Heading>
                <Heading size='md'> Collection: {imagesInfo.department_title} </Heading>
            </VStack>
        </VStack>
        


    )
}