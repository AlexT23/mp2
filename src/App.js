/* 
Grading and Goals

28% List View:
- 4% Display API items
- 8% Search bar filter
- 8% Sort by 2+ properties
- 8% Sort by ascending, descending

12% Gallery View:
- 4% Display API items
- 8% Filter changes results

38% Details View:
- 10% Item in List View takes you to Details View
- 10% Item in Gallery View takes you to Details View
- 8%  Details view holds details
- 10% Prev & Next buttons

22% Others:
- 12% Use React Router & PropTypes/TypeScript
- 10% General Design

To Do: 
- Set up general website design & links to different views. Route. [DONE]
- Set up API --> Art [DONE]
- Set up List View [PARTIAL, needs linking to details]
- Set up Gallery View [DONE - Could use more sensible filters though]
- Set up Details View + Nav Buttons
- Set up Routes between List/Gallery & Details screens
- Accredition

Extra To Dos:
- Gallery view: Change filters to sensible categories
- Cleanup appearance

*/

import llama from './Icons/Llama.png';
import bear from './Icons/Bear.png';
import goose from './Icons/Goose.png';

import './App.css';
import { ChakraProvider, Img } from '@chakra-ui/react';
import { HStack, Flex, Heading, Link as ChakraLink } from '@chakra-ui/react';
import { BrowserRouter as Router, Route, Routes, Link} from "react-router-dom"

// View Imports
import Home from './Views/Home.js';
import ListView from './Views/List.js';
import GalleryView from './Views/Gallery.js';
import DetailView from './Views/Detail.js';

// import Test from './test.js'; // Components used for testing/debugging/learning React features

function App() {
  return (
    <ChakraProvider>
        <Router>
            {/* TODO: Changing this div to flex causes bugs -> maybe try Container */}
            <div className="App"> 
                <Flex color="#0d253f" bg='#fff' flexFlow='column nowrap' justifyContent='flex-start' className="App-header">
                    <Heading>Art Institute of Chicago Virtual Gallery</Heading>
                    <Heading size='xs'> version 1.0.3</Heading>
                    <HStack width='50%' justifyContent='space-evenly'>
                        <Img src={llama} height='50px'/>
                        <Img src={bear} height='50px'/>
                        <Img src={goose} height='50px'/>
                    </HStack>
                    <h3>Display Options:</h3>
                    {/* Links */}
                    <Flex bg='yellow' flexFlow='row nowrap' justifyContent='space-around' border='2px' borderRadius='lg' width='50%'>
                        <ChakraLink as={Link} to="/mp2/">Home</ChakraLink>
                        <ChakraLink as={Link} to="/mp2/list">List</ChakraLink>
                        <ChakraLink as={Link} to="/mp2/gallery">Gallery</ChakraLink>
                    </Flex>
                    {/* Note for future use -> Make routes the root most level. React Router does magic that makes it so repeated components aren't re-rendered */}
                    {/* Routes */}
                    <Routes>
                        <Route path="/mp2/" element={<Home/>}/>
                        <Route path="/mp2/list" element={<ListView/>}/>
                        <Route path="/mp2/gallery" element={<GalleryView/>}/>
                        <Route path="/mp2/details/:imageID" element={<DetailView/>}/>

                        {/* Note: /:imageId lets element held within access imageID via useParams */}
                    </Routes>
                </Flex> 
            </div>  
        </Router>
    </ChakraProvider>
  );
}

export default App;
// Note: For detail view, pass the search parameters by URL --> See react router UseSearchParams
